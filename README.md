# Flash Openwrt Mikrotik Rb952ui 5ac2nD

un script pour flasher les RB952UI avec openWrt
ça automatise les instructions données ici : https://openwrt.org/fr/toh/mikrotik/common#avec_un_systeme_d_exploitation_linux_ou_autre_unix-like

pour lancer le script, il faut lui donner les droits d'exécution :
$chmod +x flash_openwrt_mikrotik_rb952ui-5ac2nD.py

puis l'exécuter :

$./flash_openwrt_mikrotik_rb952ui-5ac2nD.py
