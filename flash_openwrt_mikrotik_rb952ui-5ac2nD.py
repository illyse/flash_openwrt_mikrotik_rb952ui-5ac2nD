#!/usr/bin/python3
import os
import shutil
import subprocess
import sys
from pathlib import Path

# variables à changer:
IFNAME="enp0s25"
OPENWRT_INITRAMFS_IMAGE="openwrt-19.07.4-ar71xx-mikrotik-rb-nor-flash-16M-ac-initramfs-kernel.bin"
OPENWRT_SYSUPGRADE_IMAGE="openwrt-19.07.4-ar71xx-mikrotik-rb-nor-flash-16M-ac-squashfs-sysupgrade.bin"

# variables pas à ne pas modifier (a priori)
USER=os.environ['USER']
IP_ROUTEROS="192.168.88.1"
IP_OPENWRT="192.168.1.1"
BLUE='\033[0;34m'
RED='\033[0;31m'
NC='\033[0m'
SSH_OPT="-o UserKnownHostsFile=/dev/null -o LogLevel=ERROR -o StrictHostKeyChecking=no -o CheckHostIP=no -o KbdInteractiveAuthentication=yes -o PasswordAuthentication=yes"
DNSMASQ_BIN="/usr/sbin/dnsmasq"
PWD=os.getcwd()
DNSMASQ="sudo exec " + DNSMASQ_BIN + " --user=" + USER + " \\\
--no-daemon \\\
--listen-address 192.168.1.10 \\\
--bind-interfaces \\\
-p0 \\\
--dhcp-authoritative \\\
--dhcp-range=192.168.1.100,192.168.1.200 \\\
--bootp-dynamic \\\
--dhcp-boot=" + OPENWRT_INITRAMFS_IMAGE + " \\\
--log-dhcp \\\
--enable-tftp \\\
--tftp-root=\"" + PWD + "\""

print(BLUE + "Script de flash d'OpenWrt sur Mikrotik RB952ui-5ac2nD" + NC)

input(RED + "Brancher le câble d'alimentation du routeur, brancher le \
câble ethernet sur le port 2 du routeur, vérifier que l'ordinateur a bien \
reçu une IP (192.168.88.X) et appuyer sur 'ENTRÉE'" + NC)

print(BLUE + "Teste si dnsmasq, ssh, sudo sont installés" + NC)
if not os.path.isfile(DNSMASQ_BIN): sys.exit(RED + "L'exécutable \""
    + DNSMASQ_BIN + "\" est introuvable. Installer dnsmasq et relancer le \
    script" + NC)
if not shutil.which("ssh"): sys.exit(RED + "L'exécutable \"ssh\" est \
    introuvable dans $PATH. Installer ssh et relancer le script" + NC)
if not shutil.which("sudo"): sys.exit(RED + "L'exécutable \"sudo\" est \
    introuvable dans $PATH. Installer sudo et relancer le script" + NC)

print(BLUE + "Teste si sudo est fonctionnel" + NC)
try:
    subprocess.call(['sudo', 'echo', BLUE + 'sudo OK' + NC])
except:
    sys.exit(RED + "Configurer sudo et relancer le script" + NC)

print(BLUE + "Teste si les images OPENWRT sont présentes." + NC)
if not os.path.isfile(OPENWRT_INITRAMFS_IMAGE): sys.exit(RED + "Le fichier \""
    + OPENWRT_INITRAMFS_IMAGE + "\" est introuvable." + NC)
if not os.path.isfile(OPENWRT_SYSUPGRADE_IMAGE): sys.exit(RED + "Le fichier \""
    + OPENWRT_SYSUPGRADE_IMAGE + "\" est introuvable." + NC)

print(BLUE + "Teste si l'interface réseau " + IFNAME + " est configurée correctement." + NC)
if not os.path.isfile("/sys/class/net/" + IFNAME + "/operstate"): sys.exit(RED
    + "L'interface " + IFNAME + " n'existe pas.")
contents = Path("/sys/class/net/" + IFNAME + "/operstate").read_text()
contents = contents.replace("\n","")
if not contents == "up": sys.exit(RED + "L'interface " + IFNAME
    + "n'est pas configurée correctement.")

print(BLUE + "Teste si le routeur est accessible sur l'ip " + IP_ROUTEROS + NC)
try:
    subprocess.check_output("ping -c 1 " + IP_ROUTEROS, shell=True)
except:
    sys.exit(RED + "Brancher l'ordinateur au routeur, vérifier que celui-ci a reçu une IP (192.168.88.X), et relancer le script" + NC)

print(BLUE + "Copie de la clé de licence depuis le routeur" + NC)
try:
    print(BLUE + "Clé de license RouterOS du routeur: " + NC, end="")
    subprocess.run("ssh " + SSH_OPT + " admin@" + IP_ROUTEROS + " \"/system license output\"", shell=True)
    byteskey = subprocess.check_output("ssh " + SSH_OPT + " admin@" + IP_ROUTEROS + " \"/system license print\" | grep \"software-id:\" | head | sed -E 's/.*software-id: ([[:alnum:]-]+).*/\\1/'", shell=True)
    key = byteskey.decode('utf-8').replace("\n","") + ".key"
    subprocess.run("scp " + SSH_OPT + " admin@" + IP_ROUTEROS + ":/" + key + " ./", shell=True)
except:
    sys.exit(RED + "Échec de la copie de la clé de license du routeur" + NC)

print(BLUE + "Configuration du routeur pour le flash" + NC)
try:
    subprocess.run("ssh " + SSH_OPT + " admin@" + IP_ROUTEROS + " \"/system routerboard settings set boot-device=try-ethernet-once-then-nand\"", shell=True)
    subprocess.run("ssh " + SSH_OPT + " admin@" + IP_ROUTEROS + " \"/system routerboard settings set boot-protocol=dhcp\"", shell=True)
    subprocess.run("ssh " + SSH_OPT + " admin@" + IP_ROUTEROS + " \"/system routerboard settings set force-backup-booter=yes\"", shell=True)
    subprocess.run("ssh " + SSH_OPT + " admin@" + IP_ROUTEROS + " \"/system shutdown\"", shell=True)
except:
    sys.exit(RED + "Échec de la configuration du routeur pour le flash" + NC)

input(RED + "Débrancher le câble d'alimentation du routeur et appuyer sur \
'ENTRÉE'" + NC)

print(BLUE + "Configuration réseau de l'ordinateur" + NC)
try:
    subprocess.run("sudo /sbin/ip addr replace 192.168.1.10/24 dev " + IFNAME, shell=True)
    subprocess.run("sudo /sbin/ip link set dev " + IFNAME + " up", shell=True)
except:
    sys.exit(RED + "Échec de la configuration réseau de l'ordinateur" + NC)

print(BLUE + "Démarrage de dnsmasq" + NC)
try:
    dnsmasqprocess = subprocess.Popen("exec " + DNSMASQ, shell=True)
except:
    sys.exit(RED + "Échec du démarrage de dnsmasq" + NC)

input(RED + "Brancher le câble ethernet sur le port 1 du routeur \
(Internet/PoEin), rebrancher le câble d'alimentation du routeur. Attendre jusqu'à \
lire 'dnsmasq-tftp: sent " + OPENWRT_INITRAMFS_IMAGE + "' puis appuyer sur \
'ENTRÉE'" + NC)

dnsmasqprocess.kill()

input(RED + "Débrancher le câble ethernet du routeur, le brancher sur le port \
2. Attendre plusieurs minutes que le flash d'OpenWrt se termine: \
toutes les LEDs de la façade du routeur vont s'éteindre, puis une LED rouge \
va s'allumer sur le port 5, puis enfin la LED du port 2 va s'allumer en vert. \
Vérifier que l'ordinateur a bien reçu une IP (192.168.1.X) et appuyer sur \
'ENTRÉE'" + NC)

print(BLUE + "Copie de l'image sysupgrade OpenWrt" + NC)
try:
    subprocess.run("scp " + SSH_OPT + " " + OPENWRT_SYSUPGRADE_IMAGE + " root@" + IP_OPENWRT + ":/tmp", shell=True)
except:
    sys.exit(RED + "Échec de la copie de l'image sysupgrade OpenWrt" + NC)

print(BLUE + "Lancement de sysupgrade" + NC)
try:
    subprocess.run("ssh " + SSH_OPT + " root@" + IP_OPENWRT + " \"sysupgrade -n /tmp/" + OPENWRT_SYSUPGRADE_IMAGE + "\"", shell=True)
except:
    sys.exit(RED + "Échec de sysupgrade" + NC)

print(RED + "Attendre plusieurs minutes que le flash d'OpenWrt se termine: \
toutes les LEDs de la façade du routeur vont s'éteindre, puis une LED rouge \
va s'allumer sur le port 5, puis enfin la LED du port 2 va s'allumer en vert. \
C'est terminé pour ce routeur. Au suivant!" + NC)
